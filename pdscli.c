/**
 * User-space tool for NetFilter module
 *
 * @file   pdscli.c
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <stdio.h>
#include <string.h>
#include <argp.h>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"

#include "parser.tab.c"
#include "lex.yy.c"

#define PROC_FILE "/proc/pdsfw"
#define ERR_CHECK(fn, msg) do{(err = fn) && (perror(msg),0);}while(0)

error_t arg_parser(int key, char *arg, struct argp_state *state);
void join_str(char *dest, char *join_str, char **str_arr, size_t arr_size);
error_t print_rules();
void print_rule(rule_t *rule);
error_t add_rules(rule_t *rules, size_t count);
error_t load_file(char *file);
error_t delete_rule(unsigned id);
error_t delete_all_rules(void);
error_t send_command(enum commands, size_t info, void* data, size_t size);
error_t parse_rule(char *str_rule, rule_t *rule);

enum action_enum {
    NONE, PRINT, LOAD, ADD, DELETE, DELETE_ALL
};

struct arguments_struct {
    char *file;
    char rule[512];
    unsigned int id;
    enum action_enum action;
    int options;
};

size_t max_rules = ((1024 / sizeof (rule_t)) - 1);
rule_t rules[((1024 / sizeof (rule_t)) - 1)];
size_t rule_idx = 0, rules_loaded = 0;

extern FILE* yyin;

static struct argp_option opts[] = {
    {
        .name = "load",
        .key = 'f',
        .doc = "Loads rules from file.",
        .arg = "FILE",
        .group = 1
    },
    {
        .name = "print",
        .key = 'p',
        .doc = "Prints all active rules.",
        .group = 1
    },
    {
        .name = "add",
        .key = 'a',
        .doc = "Adds new rule.",
        .arg = "RULE",
    },
    {
        .name = "delete",
        .key = 'd',
        .doc = "Deletes a rule with given ID.\n"
               "Enter 'all' as ID to delete all rules.",
        .arg = "ID",
    },
    {0} // Terminator
};

static struct argp argp_conf = {
    .options = opts,
    .parser = arg_parser,
    .doc = "User-space tool for NetFilter module which allows modifications of active rules",
};

error_t arg_parser(int key, char *arg, struct argp_state *state) {

    struct arguments_struct *arguments = (struct arguments_struct*) state->input;

    switch (key) {
    case 'p':
        arguments->action = PRINT;
        arguments->options++;
        break;
    case 'f':
        arguments->action = LOAD;
        arguments->file = arg;
        arguments->options++;
        break;
    case 'a':
        arguments->action = ADD;
        arguments->options++;
        join_str(arguments->rule, " ", state->argv + (state->next - 1), state->argc - (state->next - 1));
        state->next = state->argc;
        break;
    case 'd':
        arguments->action = DELETE;
        arguments->options++;
        if (sscanf(arg, "%u", &arguments->id) != 1) {
            if(strcasecmp(arg, "all") != 0) {
                argp_error(state, "ID has to be unsigned integer or 'all'\n");
                return EINVAL;
            }
            arguments->action = DELETE_ALL;
        }
        break;
    case ARGP_KEY_END:
        if (arguments->options != 1) {
            argp_error(state, "Please specify exactly one option\n");
            return EINVAL;
        }
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

/**
 * Joins array of strings together using join_str as glue
 * @param join_str String used as glue between joined strings
 * @param str_arr Array of strings to join
 * @param arr_size Number of strings in array
 */
void join_str(char *dest, char *join_str, char **str_arr, size_t arr_size) {
    size_t join_len, i;

    if (arr_size == 0) {
        dest[0] = 0;
        return;
    }

    join_len = strlen(join_str);

    strcpy(dest, str_arr[0]);
    dest += strlen(str_arr[0]);
    for (i = 1; i < arr_size; i++) {
        strcpy(dest, join_str);
        dest += join_len;
        strcpy(dest, str_arr[i]);
        dest += strlen(str_arr[i]);
    }

    *dest = 0;
}

error_t print_rules() {
    FILE *f;
    size_t n, i;
    rule_t rule;

    f = fopen(PROC_FILE, "r");

    if (f == NULL) {
        return 1;
    }

    printf("id\taction\tsrcip\tsrcport\tdstip\tdstport\tprotocol\n");

    fread(&n, sizeof (size_t), 1, f);

    for (i = 0; i < n; i++) {
        fread(&rule, sizeof (rule_t), 1, f);
        print_rule(&rule);
    }

    fclose(f);
}

void print_rule(rule_t *rule) {
    char str[16];

    printf("%u\t%s\t", rule->id, rule_act_string[rule->act]);

    if (rule->src_ip.ip_int == 0) {
        printf("*\t");
    }
    else {
        ip2str(&rule->src_ip, str);
        printf("%s\t", str);
    }


    if (rule->src_port == -1) {
        printf("*\t");
    }
    else {
        printf("%u\t", rule->src_port);
    }


    if (rule->dst_ip.ip_int == 0) {
        printf("*\t");
    }
    else {
        ip2str(&rule->dst_ip, str);
        printf("%s\t", str);
    }


    if (rule->dst_port == -1) {
        printf("*\t");
    }
    else {
        printf("%u\t", rule->dst_port);
    }


    printf("%s\n", rule_proto_string[rule->proto]);
}

error_t add_rules(rule_t *rules, size_t count) {
    FILE *f;
    size_t write;
    command_t cmd = {
        .command = CMD_ADD
    };

    if (access(PROC_FILE, W_OK) != 0) {
        return 1;
    }

    while (count > 0) {

        f = fopen(PROC_FILE, "w");

        if (f == NULL) {
            return 1;
        }

        write = count;

        if (write > max_rules) {
            write = max_rules;
        }

        cmd.info = write;
        fwrite(&cmd, sizeof (command_t), 1, f);
        write = fwrite(rules, sizeof (rule_t), write, f);

        fclose(f);

        rules += write;
        count -= write;
    }

    return 0;
}

/**
 * Loads rules from file and sends them to kernel module
 */
error_t load_file(char *file) {
    FILE *f;

    // Check file accessibility
    if (access(file, R_OK) != 0) {
        perror(NULL);
        return 1;
    }

    if (access(PROC_FILE, W_OK) != 0) {
        return 1;
    }

    f = fopen(file, "r");

    yyin = f;

    yyparse();

    rules_loaded += rule_idx;

    printf("Rules loaded: %lu\n", rules_loaded);

    if (rule_idx > 0) {
        add_rules(rules, rule_idx);
    }

    fclose(f);

    return 0;
}

error_t delete_rule(unsigned id) {
    FILE *f;
    command_t cmd = {
        .command = CMD_DEL
    };

    if (access(PROC_FILE, W_OK) != 0) {
        return 1;
    }

    f = fopen(PROC_FILE, "w");

    if (f == NULL) {
        return 1;
    }

    printf("Removing rule: %u\n", id);

    cmd.info = id;
    fwrite(&cmd, sizeof (command_t), 1, f);

    fclose(f);
}

error_t delete_all_rules(void) {
    FILE *f;
    command_t cmd = {
        .command = CMD_DEL_ALL
    };

    if (access(PROC_FILE, W_OK) != 0) {
        return 1;
    }

    f = fopen(PROC_FILE, "w");

    if (f == NULL) {
        return 1;
    }

    printf("Removing all rules\n");

    fwrite(&cmd, sizeof (command_t), 1, f);

    fclose(f);
}

error_t parse_rule(char *str_rule, rule_t *rule) {

    printf("%s\n", str_rule);

    YY_BUFFER_STATE yy_buf;

    yy_buf = yy_scan_string(str_rule);

    yyparse();

    yy_delete_buffer(yy_buf);

    if (rule_idx > 0) {
        *rule = rules[0];
        return 0;
    }

    errno = EINVAL;

    return 1;
}

int main(int argc, char **argv) {
    error_t err;
    struct arguments_struct arguments = {0};
    rule_t rule;

    if ((err = argp_parse(&argp_conf, argc, argv, 0, 0, &arguments)) != 0) {
        return err;
    }

    switch (arguments.action) {
    case ADD:
        ERR_CHECK(parse_rule(arguments.rule, &rule), NULL);
        if (err != 0) {
            break;
        }
        ERR_CHECK(add_rules(&rule, 1), NULL);
        break;
    case DELETE:
        ERR_CHECK(delete_rule(arguments.id), NULL);
        break;
    case DELETE_ALL:
        ERR_CHECK(delete_all_rules(),NULL);
        break;
    case LOAD:
        err = load_file(arguments.file);
        if (err) {
            switch (errno) {
            case ENOENT:
                perror("Is module pdsfw loaded?");
                break;
            default:
                perror(NULL);
            }
        }
        break;
    case PRINT:
        ERR_CHECK(print_rules(), NULL);
        break;
    }

    return err;
}

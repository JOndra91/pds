obj-m+=pdsfw.o

pdscli:CFLAGS=-g
pdscli:LDFLAGS=-lfl

FIT_ARCH=xjanos12.tar.gz
FIT_FILES=$(wildcard *.c) $(wildcard *.h) doc/dokumentace.pdf parser.l parser.y Makefile

all: pdsfw.ko pdscli

pdscli: pdscli.c parser.tab.c lex.yy.c common.h
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

pdsfw.ko: pdsfw.c common.h
	$(MAKE) -C /lib/modules/$(shell uname -r)/build M="$(CURDIR)" modules

clean:
	$(MAKE) -C /lib/modules/$(shell uname -r)/build M="$(CURDIR)" clean
	rm -f pdscli parser.tab.c lex.yy.c parser.tab.h *.o *.ko $(FIT_ARCH)

parser.tab.c parser.tab.h: parser.y
	bison $< -d

lex.yy.c: parser.l
	flex $<

doc: doc/dokumentace.pdf
	cd doc/; $(MAKE) doc clean

pack: doc clean
	cd ..; tar -cvzf pds.tar.gz $(shell basename "$(CURDIR)")

fit-pack: doc clean
	tar -cvzf $(FIT_ARCH) $(FIT_FILES)

.PHONY: all clean pack doc fit-pack

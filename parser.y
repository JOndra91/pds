%{
#include <stdio.h>
#include <string.h>

#include "common.h"

extern int yyparse();
extern int yylex();
extern int yylineno;
extern char* yytext;
extern size_t max_rules, rule_idx, rules_loaded;
extern rule_t rules[];

void yyerror(char *s) {
    fprintf(stderr, "Invalid rule on line %d\n", yylineno);
}

int term;
// Příkazy jsou ukončeny znakem konce řádku,
// pro jistotu je třeba simulovat jeden nový řádek před koncem souboru.
#define yyterminate() {term--; if(term>1) return TENDLINE; if(term==1) return TEOF; return YY_NULL;};
%}

%union {
    unsigned uint;
    ip_t ip;
    proto_t proto;
    act_t act;
    rule_t rule;
    rule_t *rules;
};


%initial-action { rule_idx = 0; rules_loaded=0; term=3;}

%token <uint> TNUM
%token TALLOW
%token TDROP
%token TENDLINE
%token TEOF
%token TDOT
%token TTO
%token TFROM
%token TANY
%token TTCP
%token TUDP
%token TICMP
%token TIP
%token TSRC_PORT
%token TDST_PORT
%token TTRASH

%type <proto> protocol
%type <act> action
%type <uint> src_port dst_port
%type <rule> rule_base rule_src rule
%type <ip> ip
%type <void> line lines file

%start file

%%

file    : lines TEOF

lines   : lines line {}
        | line {}
        ;

line    : rule TENDLINE {
            $1.checksum = checksum(&($1));
            rules[rule_idx++] = $1;
            if(rule_idx == max_rules){
                add_rules(rules, rule_idx);
                rules_loaded += rule_idx;
                rule_idx = 0;
            }
        }
        | TENDLINE {} // skip empty lines
        | error TENDLINE {} // skip empty lines
        ;

rule_base   : TNUM action protocol TFROM ip TTO ip {$$.id = $1; $$.act = $2; $$.proto = $3; $$.src_ip = $5; $$.dst_ip = $7; }
            ;

rule_src    : rule_base src_port {$$.src_port = $2;}
            | rule_base {$$.src_port = -1;}
            ;

rule    : rule_src dst_port {$$.dst_port = $2;}
        | rule_src {$$.dst_port = -1;}
        ;

ip      : TNUM TDOT TNUM TDOT TNUM TDOT TNUM {$$.ip_oct[0] = $1; $$.ip_oct[1] = $3; $$.ip_oct[2] = $5; $$.ip_oct[3] = $7;}
        | TANY {$$.ip_int = 0;}
        ;

src_port: TSRC_PORT TNUM {$$=$2;}
        ;

dst_port: TDST_PORT TNUM {$$=$2;}
        ;

protocol: TTCP {$$=TCP;}
        | TUDP {$$=UDP;}
        | TICMP {$$=ICMP;}
        | TIP {$$=IP;}
        ;

action  : TALLOW {$$=ALLOW;}
        | TDROP {$$=DENY;}
        ;

%%

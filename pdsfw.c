/**
 * NetFilter module
 *
 * @file   pdsfw.c
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/skbuff.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/uaccess.h>
#include <net/ip.h>
#include <asm-generic/errno-base.h>

#define _KERN_MOD_

#include "common.h"

#define MOD_NAME "pdsfw"
#define PROMPT MOD_NAME ": "
#define STR_BUF_SIZE 1024

static int proc_fs_open(struct inode *, struct file *);
static int proc_fs_read(struct seq_file *, void *);
static ssize_t proc_fs_write(struct file *, const char __user *, size_t, loff_t *);

static unsigned int filter4(const struct nf_hook_ops *ops, struct sk_buff *skb,
                     const struct net_device *in, const struct net_device *out,
                     int (*okfn)(struct sk_buff *));

static void rule_str(rule_t *rule, char *dest_str, size_t buf_size);

static int add_rule(rule_t *);
static int del_rule(uint32_t id);
static void del_all_rules(void);

typedef struct rules_node_struct {
    rule_t rule;
    struct list_head list;
} rules_node_t;

static LIST_HEAD(rules);
static size_t ruleCount = 0;
static char *str_buf;

static struct proc_dir_entry *proc_fs_file;
static const struct file_operations proc_fs_fops = {
    .owner = THIS_MODULE,
    .open = proc_fs_open,
    .read = seq_read,
    .write = proc_fs_write,
    .release = single_release
};

static struct nf_hook_ops nf_hook_funcs[] = {
    {
        .hook = filter4,
        .hooknum = NF_INET_LOCAL_IN,
        .owner = THIS_MODULE,
        .pf = PF_INET,
        .priority = NF_IP_PRI_FILTER,
    }
};

int proc_fs_open(struct inode *i, struct file *f) {
    return single_open(f, proc_fs_read, NULL);
}

#define sbprintf(args...) do{n -= snprintf(dest_str+(buf_size-n), n, args);}while(0)

/**
 * Converts rule structure to string
 * @param rule Rule structure
 * @param dest_str String buffer
 * @param buf_size String buffer size
 */
void rule_str(rule_t *rule, char *dest_str, size_t buf_size) {
    int n = buf_size;
    unsigned char *ip;

    sbprintf("%-5u %5s %-4s", rule->id, rule_act_string[rule->act], rule_proto_string[rule->proto]);

    if (rule->src_ip.ip_int == 0) {
        sbprintf(" from       any      ");
    }
    else {
        ip = (unsigned char*) &(rule->src_ip);
        sbprintf(" from %03u.%03u.%03u.%03u", ip[0], ip[1], ip[2], ip[3]);
    }

    if (rule->dst_ip.ip_int == 0) {
        sbprintf(" to       any      ");
    }
    else {
        ip = (unsigned char*) &(rule->dst_ip);
        sbprintf(" to %03u.%03u.%03u.%03u", ip[0], ip[1], ip[2], ip[3]);
    }

    if (rule->src_port != -1) {
        sbprintf(" src-port %5u", rule->src_port);
    }

    if (rule->dst_port != -1) {
        sbprintf(" dst-port %5u", rule->dst_port);
    }
}

/**
 * Handles reading from proc_fs file.
 */
int proc_fs_read(struct seq_file *f, void *_void) {
    rules_node_t *rule_node;

    seq_write(f, &(ruleCount), sizeof (size_t));

    list_for_each_entry(rule_node, &rules, list) {
        seq_write(f, &(rule_node->rule), sizeof (rule_t));
    }

    return 0;
}

/**
 * Handles writing to proc_fs file.
 */
ssize_t proc_fs_write(struct file *f, const char __user *m, size_t s, loff_t *l) {
    size_t buf_sz = 1024;
    char *buf;
    struct command_struct *c;
    rule_t *rule;

    buf = kmalloc(buf_sz, GFP_KERNEL);

    if (buf == NULL) {
        return -ENOMEM;
    }

    if (copy_from_user(buf, m, buf_sz)) {
        return -EFAULT;
    }

    printk(KERN_DEBUG PROMPT "Write size: %lu\n", s);

    c = (struct command_struct*) buf;

    if (c->command == CMD_ADD) {

        for (rule = (rule_t*) (buf + sizeof (struct command_struct));
            c->info; c->info--, rule++) {
            add_rule(rule);
        }
    }
    else if (c->command == CMD_DEL) {
        del_rule(c->info);
    }
    else if(c->command == CMD_DEL_ALL) {
        del_all_rules();
    }

    kfree(buf);

    return s;
}

/**
 * Adds rule to rules list
 * @param r Rule structure
 * @return 0 on success
 *         EINVAL when rule is malformed (checksums doesn't match)
 *         ENOMEM when memory could not be allocated
 */
int add_rule(rule_t *r) {
    struct list_head *head = &rules;
    rules_node_t *rule_node;
    rule_t *rule;

    if (r->checksum != checksum(r)) {
        printk(KERN_DEBUG PROMPT "Malformed rule (checksums don't match)\n");
        return EINVAL;
    }

    list_for_each_entry(rule_node, &rules, list) {

        rule = (rule_t*) rule_node;

        if (rule->id > r->id) {
            head = &rule_node->list;
            break;
        }


        if (r->id == rule->id) {
            rule_str(rule, str_buf, STR_BUF_SIZE);
            printk(KERN_INFO PROMPT "Removed rule [replace]: %s\n", str_buf);

            *rule = *r;

            rule_str(rule, str_buf, STR_BUF_SIZE);
            printk(KERN_INFO PROMPT "New rule [replace]: %s\n", str_buf);

            return 0;
        }
    }

    rule_node = kmalloc(sizeof (rules_node_t), GFP_KERNEL);

    if (rule_node == NULL) {
        printk(KERN_WARNING PROMPT "Could not allocate memory for new rule\n");
        return ENOMEM;
    }

    rule_node->rule = *r;

    INIT_LIST_HEAD(&rule_node->list);

    list_add_tail(&rule_node->list, head);

    rule_str(r, str_buf, STR_BUF_SIZE);
    printk(KERN_INFO PROMPT "New rule: %s\n", str_buf);

    ruleCount++;

    return 0;
}

/**
 * Removes rule from rules list
 * @param id Rule id
 * @return 0 on success
 *         EINVAL when rule with given id is not in the list.
 */
int del_rule(uint32_t id) {
    rules_node_t *pos, *tmp;

    if (!list_empty(&rules)) {

        list_for_each_entry_safe(pos, tmp, &rules, list) {

            if (pos->rule.id == id) {
                rule_str(&pos->rule, str_buf, STR_BUF_SIZE);

                list_del(&pos->list);
                kfree(pos);

                printk(KERN_INFO PROMPT "Removed rule: %s\n", str_buf);

                ruleCount--;

                return 0;
            }

            if (pos->rule.id > id) {
                break;
            }
        }
    }

    printk(KERN_INFO PROMPT "Rule %u not found\n", id);
    return EINVAL;
}

/**
 * Clears rules list
 */
void del_all_rules(void) {
    rules_node_t *pos, *tmp;

    list_for_each_entry_safe(pos, tmp, &rules, list) {
        list_del(&pos->list);
        kfree(pos);
    }

    ruleCount = 0;
}

/**
 * NetFilter IP4 hook
 */
unsigned int filter4(const struct nf_hook_ops *ops, struct sk_buff *skb,
                     const struct net_device *in, const struct net_device *out,
                     int (*okfn)(struct sk_buff *)) {

    rules_node_t *rule_node;
    rule_t *rule;
    struct iphdr *ip_header;
    struct tcphdr *tcp_header;
    struct udphdr *udp_header;
    proto_t proto;
    uint16_t src_port, dst_port;

    ip_header = (struct iphdr*) skb_network_header(skb);

    switch (ip_header->protocol) {
    case IPPROTO_ICMP:
        proto = ICMP;
        src_port = -1;
        dst_port = -1;
        break;
    case IPPROTO_TCP:
        proto = TCP;
        tcp_header = tcp_hdr(skb);
        src_port = htons(tcp_header->source);
        dst_port = htons(tcp_header->dest);
        break;
    case IPPROTO_UDP:
        proto = UDP;
        udp_header = udp_hdr(skb);
        src_port = htons(udp_header->source);
        dst_port = htons(udp_header->dest);
        break;
    default:
        proto = -1;
        src_port = 0;
        dst_port = 0;
    }

    list_for_each_entry(rule_node, &rules, list) {

        rule = (rule_t*) rule_node;

        if ((rule->proto & proto) == 0)
            continue;

        if (rule->src_ip.ip_int != 0 && rule->src_ip.ip_int != ip_header->saddr)
            continue;

        if (rule->dst_ip.ip_int != 0 && rule->dst_ip.ip_int != ip_header->daddr)
            continue;

        if (rule->src_port != -1 && rule->src_port != src_port)
            continue;

        if (rule->dst_port != -1 && rule->dst_port != dst_port)
            continue;

        // printk(KERN_DEBUG PROMPT "Packet matched rule #%u <%s>\n", rule->id, rule_act_string[rule->act]);

        return rule->act;
    }

    return NF_ACCEPT;
}

int __init pdsfw_init(void) {

    proc_fs_file = proc_create(MOD_NAME, 0644, NULL, &proc_fs_fops);

    if (proc_fs_file == NULL) {
        printk(KERN_ALERT PROMPT "Could not initialize /proc/%s\n", MOD_NAME);
        return -ENOMEM;
    }

    str_buf = kmalloc(sizeof (char)*STR_BUF_SIZE, GFP_KERNEL);

    if (str_buf == NULL) {
        proc_remove(proc_fs_file);
        return -ENOMEM;
    }

    nf_register_hooks(nf_hook_funcs, sizeof (nf_hook_funcs) / sizeof (struct nf_hook_ops));

    return 0;
}

void __exit pdsfw_exit(void) {

    del_all_rules();

    list_del(&rules);

    nf_unregister_hooks(nf_hook_funcs, sizeof (nf_hook_funcs) / sizeof (struct nf_hook_ops));
    kfree(str_buf);
    proc_remove(proc_fs_file);
}


MODULE_DESCRIPTION("NetFilter module");
MODULE_AUTHOR("Ondrej Janosik <xjanos12@stud.fit.vutbr.cz>");
MODULE_LICENSE("None");
module_init(pdsfw_init);
module_exit(pdsfw_exit);

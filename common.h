/*
 * Common types for NetFilter module and NetFilter module client
 *
 * @file   common.h
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#ifndef COMMON_H
#define	COMMON_H

#ifndef _KERN_MOD_
#include <linux/kernel.h>
#include <linux/in.h>
#include <linux/in6.h>
#include <linux/netfilter.h>
#include <stddef.h>
#include <stdint.h>
#endif // _KERN_MOD_

#ifdef	__cplusplus
extern "C" {
#endif

    enum commands {
        CMD_ADD = (1 << 13),
        CMD_DEL = (1 << 18),
        CMD_DEL_ALL = (1 << 21)
    };

    typedef struct command_struct {
        enum commands command;
        // Count of rule in case of CMD_ADD
        // ID of rule to delete in case of CMD_DEL
        size_t info;
    } command_t;

    typedef enum rule_act_struct {
        ALLOW = NF_ACCEPT, // NF_ACCEPT
        DENY = NF_DROP // NF_DROP
    } act_t;

    typedef enum rule_proto_struct {
        TCP = 1,
        UDP = 2,
        ICMP = 4,
        IP = 7,
    } proto_t;

    typedef union ip_struct {
        uint32_t ip_int;
        unsigned char ip_oct[4];
    } ip_t;

    typedef struct rule_struct {
        uint32_t id;
        act_t act;
        proto_t proto;
        ip_t src_ip;
        ip_t dst_ip;
        //        uint32_t src_mask;
        //        uint32_t dst_mask;
        uint32_t src_port;
        uint32_t dst_port;
        uint32_t checksum;
    } rule_t;

    /**
     * Computes rule's checksum
     * @param rule
     * @return Checksum
     */
    static inline uint32_t checksum(rule_t *rule) {
        uint32_t sum;

        sum = rule->id ^ rule->src_ip.ip_int ^ rule->dst_ip.ip_int;
        sum = sum ^ rule->src_port ^ rule->dst_port;
        sum += (rule->proto << 3) + (rule->act << 12);

        return sum;
    }

    /**
     * Converts ip_t type to string.
     * Buffer size have to be at least 16.
     * @param ip IP address to convert
     * @param str String buffer
     */
    static inline void ip2str(ip_t *ip, char *str) {
        snprintf(str, 16, "%u.%u.%u.%u",
                ip->ip_oct[0], ip->ip_oct[1], ip->ip_oct[2], ip->ip_oct[3]);
    }

    static char *rule_act_string[] = {
        [ALLOW] = "allow",
        [DENY] = "deny",
    };

    static char *rule_proto_string[] = {
        [0] = "unknown",
        [TCP] = "tcp",
        [UDP] = "udp",
        [ICMP] = "icmp",
        [IP] = "ip"
    };


#ifdef	__cplusplus
}
#endif

#endif	/* COMMON_H */

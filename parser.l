%option yylineno
%{
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "parser.tab.h"

#ifdef DEBUG
#define T(x) printf("%s[%s]\n", #x, yytext); return T##x
#else
#define T(x) return T##x
#endif

%}

%%

[ \t]           ;

[0-9]+          {sscanf(yytext, "%u", &yylval.uint); T(NUM);};

(?i:allow)      {T(ALLOW);};
(?i:deny)       {T(DROP);};

(?i:tcp)        {T(TCP);};
(?i:udp)        {T(UDP);};
(?i:ip)         {T(IP);};
(?i:icmp)       {T(ICMP);};

(?i:from)       {T(FROM);};
(?i:to)         {T(TO);};
(?i:any)        {T(ANY);};
(?i:src-port)   {T(SRC_PORT);};
(?i:dst-port)   {T(DST_PORT);};

\.              {T(DOT);};
\n              {T(ENDLINE);};
#.*$            ; // Comments

.               {T(TRASH);};


%%
